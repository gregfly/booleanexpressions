#include "compareexpressionsitemmodel.h"

#include "expression.h"
#include <QtMath>

CompareExpressionsItemModel::CompareExpressionsItemModel(QObject * parent) : QAbstractItemModel(parent)
{
    this->rootExpressions.clear();
}

CompareExpressionsItemModel::~CompareExpressionsItemModel()
{
    qDeleteAll(this->rootExpressions);
    this->rootExpressions.clear();
}

void CompareExpressionsItemModel::setRootExpressions(QList<Expression *> list)
{
    this->beginResetModel();
    qDeleteAll(this->rootExpressions);
    this->rootExpressions.clear();
    this->rootExpressions = list;
    this->endResetModel();
}

QModelIndex CompareExpressionsItemModel::index(int row, int column, const QModelIndex &/*parent*/) const
{
    return this->createIndex(row, column, row * column);
}

QModelIndex CompareExpressionsItemModel::parent(const QModelIndex & /* child */) const
{
    return QModelIndex();
}

int CompareExpressionsItemModel::rowCount(const QModelIndex & /* parent */) const
{
    if (this->rootExpressions.isEmpty()) {
        return 0;
    }
    int count = this->variables().count();
    return count > 0? qPow(2, this->variables().count()) : 0;
}

int CompareExpressionsItemModel::columnCount(const QModelIndex & /* parent */) const
{
    if (this->rootExpressions.isEmpty()) {
        return 0;
    }
    int count = this->variables().count();
    return count > 0? count + this->rootExpressions.count() : 0;
}

QVariant CompareExpressionsItemModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::TextAlignmentRole) {
        return (int)(Qt::AlignRight | Qt::AlignVCenter);
    }
    if (this->rootExpressions.isEmpty()) {
        return QVariant();
    }
    const QVariantMap &varValues = this->rootExpressions.first()->values(index.row());
    if (role == Qt::DisplayRole) {
        QStringList vars = this->variables();
        if (vars.isEmpty()) {
            return QVariant();
        }
        int varCount = vars.count();
        if (index.column() < varCount) {
            return varValues[vars.at(index.column())];
        }
        return this->rootExpressions.at(index.column() - varCount)->execute(varValues);
    }
    if (role == Qt::UserRole) {
        bool r = this->rootExpressions.first()->execute(varValues).toBool();
        foreach (const Expression * expression, this->rootExpressions) {
            if (r != expression->execute(varValues).toBool()) {
                return 0;
            }
        }
        return 1;
    }
    return QVariant();
}

QVariant CompareExpressionsItemModel::headerData(int section,
                                  Qt::Orientation orientation,
                                  int role) const
{
    if (role != Qt::DisplayRole) {
        return QVariant();
    }
    if (orientation == Qt::Vertical) {
        return section;
    }
    int varCount = this->variables().count();
    if (section < varCount) {
        return this->variables().at(section);
    }
    return this->rootExpressions.at(section - varCount)->toString();
}

QStringList CompareExpressionsItemModel::variables() const
{
    if (this->rootExpressions.isEmpty()) {
        return QStringList();
    }
    QList<Expression *>::ConstIterator it = this->rootExpressions.begin();
    QStringList list = it.i->t()->variables();
    while (++it != this->rootExpressions.end()) {
        QStringList vars = it.i->t()->variables();
        if (list.count() != vars.count()) {
            return QStringList();
        }
        foreach (const QString &str, vars) {
            if (!list.contains(str)) {
                return QStringList();
            }
        }
    }
    return list;
}
