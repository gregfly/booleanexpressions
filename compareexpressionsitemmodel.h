#ifndef COMPAREEXPRESSIONSITEMMODEL_H
#define COMPAREEXPRESSIONSITEMMODEL_H

#include <QAbstractItemModel>

class Expression;

class CompareExpressionsItemModel : public QAbstractItemModel
{
public:
    explicit CompareExpressionsItemModel(QObject * parent = 0);
    virtual ~CompareExpressionsItemModel();

    void setRootExpressions(QList<Expression *>);

    QModelIndex index(int row, int column,
                      const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &child) const;

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role) const;

    QStringList variables() const;

private:
    QList<Expression *> rootExpressions;
};

#endif // COMPAREEXPRESSIONSITEMMODEL_H
