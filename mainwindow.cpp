#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QStandardItemModel>
#include <QStandardItem>
#include <QInputDialog>
#include <QMessageBox>
#include <QAction>
#include <QTableView>
#include <QHeaderView>
#include <QSortFilterProxyModel>
#include <QStringListModel>
#include "compareexpressionsitemmodel.h"
#include "expressionitemmodel.h"
#include "expression.h"
#include "booleanparser.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    booleanParser(new BooleanParser())
{
    this->listModel = new QStandardItemModel(this);
    this->ui->setupUi(this);
    this->ui->listView->setModel(this->listModel);
    this->ui->tableView->horizontalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);

    QObject::connect(this->ui->listView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), this, SLOT(refreshModel()));

    QAction * removeAction = new QAction(tr("Удалить"), this);
    QObject::connect(removeAction, SIGNAL(triggered(bool)), this, SLOT(removeExpression()));
    this->ui->listView->addAction(removeAction);

    this->setWindowState(Qt::WindowMaximized);
}

MainWindow::~MainWindow()
{
    delete this->ui;
    delete this->booleanParser;
}

void MainWindow::addExpression()
{
    bool ok;
    QString str = QInputDialog::getText(this, this->windowTitle(), tr("Введите логическое выражение"), QLineEdit::Normal, QString(), &ok);
    if (!ok || str.isEmpty()) {
        return;
    }
    this->listModel->appendRow(new QStandardItem(str));
}

void MainWindow::removeExpression()
{
    QList<int> indexes;
    foreach (QModelIndex index, this->ui->listView->selectionModel()->selectedRows()) {
        indexes.prepend(index.row());
    }
    foreach (int i, indexes) {
        this->listModel->removeRow(i);
    }
}

void MainWindow::infoExpression()
{
    QAbstractItemModel * model = this->ui->tableView->model();
    if (!model) {
        return;
    }
    int count = 0;
    int rowCount = model->rowCount();
    int column = model->columnCount() - 1;
    for (int row = rowCount - 1; row >= 0; --row) {
        model->index(row, column).data().toBool() && ++count;
    }
    QMessageBox::information(this, this->windowTitle(), tr("Количество 0: %2; Количество 1: %1.").arg(count).arg(rowCount - count));
}

void MainWindow::refreshModel()
{
    if (this->ui->tableView->model()) {
        this->ui->tableView->model()->deleteLater();
    }
    QModelIndexList indexes = this->ui->listView->selectionModel()->selectedRows();
    if (indexes.isEmpty()) {
        this->ui->tableView->setModel(NULL);
    } else if (indexes.count() > 1) {
        QList<Expression *> expressions;
        foreach (const QModelIndex &index, indexes) {
            expressions.append(new Expression(this->booleanParser->parse(index.data().toString())));
        }
        QSortFilterProxyModel * proxyModel = new QSortFilterProxyModel(this);
        proxyModel->setFilterRole(Qt::UserRole);
        proxyModel->setFilterFixedString("1");
        proxyModel->setFilterKeyColumn(0);
        CompareExpressionsItemModel * model = new CompareExpressionsItemModel(proxyModel);
        model->setRootExpressions(expressions);
        proxyModel->setSourceModel(model);
        this->ui->tableView->setModel(proxyModel);
    } else {
        ExpressionItemModel * model = new ExpressionItemModel(this);
        model->setRootExpression(new Expression(this->booleanParser->parse(indexes.first().data().toString())));
        this->ui->tableView->setModel(model);
    }
}

void MainWindow::aboutQt()
{
    QApplication::aboutQt();
}
