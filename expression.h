#ifndef EXPRESSION_H
#define EXPRESSION_H

#include <QVariantMap>
#include <QString>

class Node;

class Expression
{
public:
    Expression(Node *);
    ~Expression();

    QStringList variables() const;

    QVariant execute(const QVariantMap &vars) const;
    QString toString() const;

    QList<Expression> allSubExpressions() const;

    const QVariantMap values(int row);

private:
    Node * rootNode;
    bool isDelete;

    Expression(Node *, bool);

    static void _variables(QStringList &, Node *);
    static void _allSubExpressions(QList<Expression> &, Node *);
    static bool _execute(Node *, const QVariantMap &vars);
};

#endif // EXPRESSION_H
