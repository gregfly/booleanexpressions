#include "expressionitemmodel.h"

#include "expression.h"
#include <QtMath>

ExpressionItemModel::ExpressionItemModel(QObject *parent) : QAbstractItemModel(parent)
{
    this->rootExpression = NULL;
}

ExpressionItemModel::~ExpressionItemModel()
{
    if (this->rootExpression) {
        delete this->rootExpression;
    }
    this->rootExpression = NULL;
}

void ExpressionItemModel::setRootExpression(Expression * exp)
{
    this->beginResetModel();
    if (this->rootExpression) {
        delete this->rootExpression;
    }
    this->rootExpression = exp;
    this->endResetModel();
}

QModelIndex ExpressionItemModel::index(int row, int column, const QModelIndex &/*parent*/) const
{
    return this->createIndex(row, column, row * column);
}

QModelIndex ExpressionItemModel::parent(const QModelIndex & /* child */) const
{
    return QModelIndex();
}

int ExpressionItemModel::rowCount(const QModelIndex & /* parent */) const
{
    if (!this->rootExpression) {
        return 0;
    }
    return qPow(2, this->rootExpression->variables().count());
}

int ExpressionItemModel::columnCount(const QModelIndex & /* parent */) const
{
    if (!this->rootExpression) {
        return 0;
    }
    return this->rootExpression->variables().count() + this->rootExpression->allSubExpressions().count();
}

QVariant ExpressionItemModel::data(const QModelIndex &index, int role) const
{
    if (role == Qt::TextAlignmentRole) {
        return (int)(Qt::AlignRight | Qt::AlignVCenter);
    }
    if (role != Qt::DisplayRole || !this->rootExpression) {
        return QVariant();
    }
    QStringList vars = this->rootExpression->variables();
    const QVariantMap &varValues = this->rootExpression->values(index.row());
    int varCount = vars.count();
    if (index.column() < varCount) {
        return varValues[vars.at(index.column())];
    }
    return this->rootExpression->allSubExpressions().at(index.column() - varCount).execute(varValues);
}

QVariant ExpressionItemModel::headerData(int section,
                                  Qt::Orientation orientation,
                                  int role) const
{
    if (role != Qt::DisplayRole) {
        return QVariant();
    }
    if (orientation == Qt::Vertical) {
        return section;
    }
    int varCount = this->rootExpression->variables().count();
    if (section < varCount) {
        return this->rootExpression->variables().at(section);
    }
    return this->rootExpression->allSubExpressions().at(section - varCount).toString();
}
