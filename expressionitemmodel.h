#ifndef EXPRESSIONITEMMODEL_H
#define EXPRESSIONITEMMODEL_H

#include <QAbstractItemModel>

class Expression;

class ExpressionItemModel : public QAbstractItemModel
{
public:
    explicit ExpressionItemModel(QObject * parent = 0);
    virtual ~ExpressionItemModel();

    void setRootExpression(Expression *);

    QModelIndex index(int row, int column,
                      const QModelIndex &parent) const;
    QModelIndex parent(const QModelIndex &child) const;

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role) const;

private:
    Expression * rootExpression;
};

#endif // EXPRESSIONITEMMODEL_H
