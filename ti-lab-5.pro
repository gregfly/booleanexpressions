#-------------------------------------------------
#
# Project created by QtCreator 2015-11-22T13:51:27
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ti-lab-5
TEMPLATE = app

HEADERS       = booleanmodel.h \
                booleanparser.h \
                mainwindow.h \
                node.h \
    expressionitemmodel.h \
    expression.h \
    compareexpressionsitemmodel.h

SOURCES       = booleanmodel.cpp \
                booleanparser.cpp \
                mainwindow.cpp \
                main.cpp \
                node.cpp \
    expressionitemmodel.cpp \
    expression.cpp \
    compareexpressionsitemmodel.cpp

FORMS    += mainwindow.ui
