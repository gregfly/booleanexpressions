#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class QStandardItemModel;
class BooleanParser;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    virtual ~MainWindow();

public slots:
    void addExpression();
    void removeExpression();
    void infoExpression();
    void refreshModel();
    void aboutQt();

private:
    Ui::MainWindow * ui;
    QStandardItemModel * listModel;
    BooleanParser * booleanParser;
};

#endif // MAINWINDOW_H
