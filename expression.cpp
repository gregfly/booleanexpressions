#include "expression.h"

#include "node.h"

#include <QtMath>

Expression::Expression(Node * node) : rootNode(node)
{
    this->isDelete = true;
}

Expression::~Expression()
{
    if (this->rootNode && this->isDelete) {
        delete this->rootNode;
    }
    this->rootNode = NULL;
}

QStringList Expression::variables() const
{
    QStringList vars;
    Expression::_variables(vars, this->rootNode);
    return vars;
}

QVariant Expression::execute(const QVariantMap &vars) const
{
    return (int)Expression::_execute(this->rootNode, vars);
}

QString Expression::toString() const
{
    return this->rootNode->str;
}

QList<Expression> Expression::allSubExpressions() const
{
    QList<Expression> list;
    Expression::_allSubExpressions(list, this->rootNode);
    return list;
}

const QVariantMap Expression::values(int row)
{
    QVariantMap values;
    QStringList vars = this->variables();
    int i = vars.count();
    foreach (QString var, vars) {
        int tmp = qPow(2, i--);
        for (int v = 0, k = 0; k <= row; ++k) {
            if (k == row) {
                values[var] = v < tmp / 2.0? 0 : 1;
            }
            if (++v >= tmp) {
                v = 0;
            }
        }
    }
    return values;
}

Expression::Expression(Node * rootNode, bool isDelete) : rootNode(rootNode), isDelete(isDelete)
{
    //
}

void Expression::_variables(QStringList &list, Node * parent)
{
    if (parent->type == Node::Identifier && !list.contains(parent->str)) {
        list.append(parent->str);
    }
    foreach (Node * node, parent->children) {
        Expression::_variables(list, node);
    }
}

void Expression::_allSubExpressions(QList<Expression> &list, Node * parent)
{
    foreach (Node * node, parent->children) {
        Expression::_allSubExpressions(list, node);
    }
    if (parent->type != Node::Root && parent->type != Node::Identifier && parent->type != Node::Operator && parent->type != Node::Punctuator && parent->type != Node::Atom) {
        Expression expr(parent, false);
        list.append(expr);
    }
}

bool Expression::_execute(Node * parent, const QVariantMap &vars)
{
    if (parent->type == Node::Identifier) {
        return vars[parent->str].toBool();
    }
    if (parent->type == Node::NotExpression) {
        return parent->children.count() % 2 == 0? !Expression::_execute(parent->children.last(), vars) : Expression::_execute(parent->children.last(), vars);
    }
    QVariant r;
    foreach (Node * node, parent->children) {
        if (node->type == Node::Punctuator || node->type == Node::Operator) {
            continue;
        }
        if (!r.isValid()) {
            r = Expression::_execute(node, vars);
            continue;
        }
        switch (parent->type) {
        case Node::OrExpression:
            r = r.toBool() || Expression::_execute(node, vars);
            break;
        case Node::AndExpression:
            r = r.toBool() && Expression::_execute(node, vars);
            break;
        case Node::ImplicationExpression:
            r = !r.toBool() || Expression::_execute(node, vars);
            break;
        default:
            break;
        }
    }
    return r.toBool();
}
